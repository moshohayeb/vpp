#!/usr/bin/env bash

BIN_DIR=$(ps -aux | grep vpp | grep interactive | awk '{print $11}' | awk '{print $1 " " length($1)}' | sort -nrk2,2 | awk '{print $1}' | head -1 | xargs dirname)

SFILE='/tmp/__vpp_dashboard_stats'

rx_interface_index=1
tx_interface_index=2

d_window=1

while [ true ]; do
    clear

    printf "\n\n\n\n\n\n\n\n\n"

    $BIN_DIR/vpp_get_stats dump > $SFILE

    os_rx_packets=$(cat /sys/class/net/os_server/statistics/rx_packets)
    os_tx_packets=$(cat /sys/class/net/os_client/statistics/tx_packets)

    rx_packets=$(cat $SFILE | grep 'if/rx$' | grep "\[$rx_interface_index " | awk '{print $4}' | awk '{sum+=$1} END { print sum }')
    tx_packets=$(cat $SFILE | grep 'if/tx$' | grep "\[$tx_interface_index " | awk '{print $4}' | awk '{sum+=$1} END { print sum }')

    rx_bytes=$(cat $SFILE | grep 'if/rx$' | grep "\[$rx_interface_index " | awk '{print $6}' | awk '{sum+=$1} END { print sum }')
    tx_bytes=$(cat $SFILE | grep 'if/tx$' | grep "\[$tx_interface_index " | awk '{print $6}' | awk '{sum+=$1} END { print sum }')

    rx_diff_packets=$((rx_packets - _rx_packets))
    tx_diff_packets=$((tx_packets - _tx_packets))

    rx_diff_bytes=$((rx_bytes - _rx_bytes))
    tx_diff_bytes=$((tx_bytes - _tx_bytes))

    rx_pps=$((rx_diff_packets / d_window))
    tx_pps=$((tx_diff_packets / d_window))

    os_rx_pps=$((os_rx_packets - _os_rx_packets))
    os_tx_pps=$((os_tx_packets - _os_tx_packets))

    rx_throughput=$((rx_diff_bytes / d_window * 8))
    tx_throughput=$((tx_diff_bytes / d_window * 8))

    rx_throughput_string=$(echo $rx_throughput | numfmt --to=si --suffix=bps --format="%.2f")
    tx_throughput_string=$(echo $tx_throughput | numfmt --to=si --suffix=bps --format="%.2f")

    printf "os_tx_pps = %d\n" $os_tx_pps
    printf "os_rx_pps = %d\n" $os_rx_pps
    echo
    printf "rx_pps = %d\n" $rx_diff_packets
    printf "tx_pps = %d\n" $tx_diff_packets
    echo
    printf "rx_throughput = %s\n" $rx_throughput_string
    printf "tx_throughput = %s\n" $tx_throughput_string
    echo
    cat $SFILE | grep vec

    _rx_packets=$rx_packets
    _tx_packets=$tx_packets
    _rx_bytes=$rx_bytes
    _tx_bytes=$tx_bytes
    _os_rx_packets=$os_rx_packets
    _os_tx_packets=$os_tx_packets

    sleep $d_window
done
