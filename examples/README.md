
# UDP Server Client

``` sh
# Server
socat -d -d  udp4-recvfrom:4000,fork exec:udp-server.sh!!/tmp/udp-requests.txt,append,creat


# Client
socat udp4-sendto:10.10.10.43:4000 exec:udp-client.sh!!/tmp/udp-responses.txt,creat,append
seq 10 | parallel -N0 'socat udp4-sendto:10.10.200.200:3000 exec:udp-client.sh!!/tmp/udp-responses.txt,creat,append'

```
