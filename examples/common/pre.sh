#!/usr/bin/env bash

rmmod ip_gre
ip netns | awk '{print $1}' | parallel "ip netns del {}"
ip link show type veth | grep qdisc | awk '{print $2}' | awk -F@ '{print $1}' | parallel "ip link del"
ip link del br0
