#!/usr/bin/env bash

ip netns | awk '{print $1}' | parallel 'ip netns exec {} bash -c "echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6"'
ip link show type veth | grep qdisc | awk '{print $2}' | awk -F@ '{print $1}' | parallel "echo 1 > /proc/sys/net/ipv6/conf/{}/disable_ipv6"
