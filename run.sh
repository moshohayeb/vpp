#!/bin/bash
#

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

set -e

if [ ! -d "$VPP_ROOT" ] || [ ! -d "$VPP_CONTRIB" ]; then
    echo "VPP_ROOT or VPP_CONTRIB are not defined"
    exit 1
fi

T=$VPP_ROOT/build-root/build-vpp_debug-native/vpp/
if [ ! -d "$T" ]; then
    echo "No VPP build found at $T"
    echo "Build it first cd $VPP_ROOT && make build"
    exit 1
fi

/bin/rm -f /dev/hugepages/rtmem*
/bin/rm -f /dev/shm/db /dev/shm/global_vm /dev/shm/vpe-api
make -C $VPP_ROOT build

if [ "$1" == "gdb" ]; then
    # cgdb -- --args $T/bin/vpp  -c  $VPP_CONTRIB/conf/gdb.conf
    # $DIR/bin/gdbgui -r --project /opt/vpp --args $T/bin/vpp unix interactive
    gdb -ex "handle SIGUSR1 noprint nostop" --args $T/bin/vpp " unix { interactive cli-listen /run/vpp/cli.sock gid 0  }   "
else
    $T/bin/vpp -c $VPP_CONTRIB/conf/HEAD
    # $T/bin/vpp unix interactive
fi
