#!/usr/bin/env bash


# nonce=$RANDOM
# nonce=$(date +%s%N)
nonce=$(uuidgen)
ip addr | grep inet | grep -v 127.0.0.1 | awk  '{print $2}' | awk -v nonce="$nonce" -F'/' '{print $1 " " nonce}' >> /tmp/udp-responses.txt
