#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

pre=$DIR/../examples/common/pre.sh
post=$DIR/../examples/common/post.sh





sh $pre >/dev/null 2>&1

ip netns add ns1
ip netns add ns2

ip link add name veth1-ns type veth peer name veth1-host
ip link add name veth2-ns type veth peer name veth2-host

ip link set dev veth1-ns netns ns1
ip link set dev veth2-ns netns ns2


ip -n ns1 addr add 10.10.100.10/24 dev veth1-ns
ip -n ns1 link set up lo
ip -n ns1 link set up veth1-ns

ip -n ns2 addr add 10.10.100.11/24 dev veth2-ns
ip -n ns2 link set up lo
ip -n ns2 link set up veth2-ns

ip link set up veth1-host
ip link set up veth2-host
ip link set up as2
ip link add name br0 type bridge
ip link set up br0
ip link set veth1-host master br0
ip link set veth2-host master br0

sh $post >/dev/null 2>&1

ip netns exec ns1 ping 10.10.100.11 -c 3
